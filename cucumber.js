module.exports = {
    // default: `--format-options '{"snippetInterface": "synchronous"}'`
    default: {
      snippetInterface: "synchronous",
      require: ["features/", "steps/"],
      format: ["@cucumber/pretty-formatter"],
      formatOptions: {
        enableColors: true,
        theme: {
          'step text': 'green',
          'scenario name':["green","underline"]
        }
      }
    }
  }

