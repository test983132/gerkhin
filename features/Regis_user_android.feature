Feature: Register

Background:
    Given User already opened "Secondhand" app

Scenario: To ensure new user can create an account
    When User clicks "Akun"
    And User clicks "Masuk"
    And User clicks "Daftar"
    And User fills Name field with "deni"
    And User fills Email field with "deni@email.com"
    And User fills Password field with "deni12345"
    And User fills Nomor HP field with "081212345678"
    And User fills Kota field with "Bali"
    And User fills Alamat field with "jalan kenangan No.22"
    And User clicks "Daftar"
    Then User successfully creates an account and is redirected to the account menu

Scenario: To ensure new user can't create an account with no name
    When User clicks "Akun"
    And User clicks "Masuk"
    And User clicks "Daftar"
    And User fills Name field with " "
    And User fills Email field with "desi@email.com"
    And User fills Password field with "desi12345"
    And User fills Nomor HP field with "081212345679"
    And User fills Kota field with "Bali"
    And User fills Alamat field with "jalan kenangan No.22"
    And User clicks "Daftar"
    Then The warning appears saying "nama tidak boleh kosong" on Nama Field

Scenario: To ensure new user can't create an account with no email
    When User clicks "Akun"
    And User clicks "Masuk"
    And User clicks "Daftar"
    And User fills Name field with "desi"
    And User fills Email field with " "
    And User fills Password field with "desi12345"
    And User fills Nomor HP field with "081212345679"
    And User fills Kota field with "Bali"
    And User fills Alamat field with "jalan kenangan No.22"
    And User clicks "Daftar"
    Then The warning appears saying "Email tidak boleh kosong" on Email field

Scenario: To ensure new user can't create an account with invalid email
    When User clicks "Akun"
    And User clicks "Masuk"
    And User clicks "Daftar"
    And User fills Name field with "desi"
    And User fills Email field with "desi.email.com"
    And User fills Password field with "desi12345"
    And User fills Nomor HP field with "081212345679"
    And User fills Kota field with "Bali"
    And User fills Alamat field with "jalan kenangan No.22"
    And User clicks "Daftar"
    Then The warning appears saying "Email tidak valid" on Email Field

 Scenario: To ensure new user can't create an account with existing email
    When User clicks "Akun"
    And User clicks "Masuk"
    And User clicks "Daftar"
    And User fills Name field with "desi"
    And User fills Email field with "deni@email.com"
    And User fills Password field with "desi12345"
    And User fills Nomor HP field with "081212345679"
    And User fills Kota field with "Bali"
    And User fills Alamat field with "jalan kenangan No.22"
    And User clicks "Daftar"
    Then The pop-up appears saying "Email sudah digunakan"