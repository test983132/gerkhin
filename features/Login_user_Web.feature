Feature: Login
    Background: user is in login page and input valid email
        Given user is in login page
        When user input valid email

    Scenario: user can login using valid crendentials
        When user input valid password
        And user click Login button
        Then user can successfuly login
        And user is redirected to the Homepage

    Scenario: user can not using wrong password
        When user input wrong password
        And user click login button
        Then user can not log in 
        And user can see error massage "invalid email or password" 
    
    Scenario: user can not using empty password
        When user leave password field empty
        And user click login button
        Then user can not log in 
        And user can see error massage "invalid email or password"

    Background: user is in login page and input valid email
        Given user is in login page
        When user input valid password

    Scenario: user can not using wrong email
        When user input wrong email
        And user click login button
        Then user can not log in 
        And user can see error massage "invalid email or password" 

    Scenario: user can not using empty email
        When user leave email field empty
        And user click login button
        Then user can not log in 
        And user can see error massage "invalid email or password"