Feature: Seller Product


    Feature Product
    Background: Given User in homepage

@Positive_Case
Scenario: User wants to add product

        When Start to type your When step here User click "+Jual"
        And User fill in the fields correctly
        And User click "Terbitkan"
        And User successfully publish products
        Then User has successfully published the product

@Negative_Case
Scenario: User wants to add product

        When Start to type your When step here User click "+Jual"
        And User fill in the fields with the minus price column
        And User click "Terbitkan"
        And User successfully publish products
        Then User can't publish products with minus prices

@Positive_Case
Scenario: User wants see own product

        When User click button "Daftar Jual Saya"
        And User direct to landing page Daftar Jual Saya
        And User click kategori "Terjual"
        And User click kategori "Semua Produk"
        And User click "Tambah Produk"
        Then User can see all categories

@Positive_Case
Scenario: User wants to receive bid requests from buyer

        When User click "Daftar Jual Saya"
        And User click kategori "Diminati"
        And User click "Terima"
        And User direct to "Hubungi di (whatsapp)"
        Then User as seller can accepting bid the product

@Positive_Case
Scenario: User wants to change sale status

        When User click "Notification"
        And User direct to info penawar and click "status"
        And User direct to perbaharui status penjualan produkmu
        And User choose berhasil terjual and click "Kirim"
        And User direct to info penawar and change status product
        Then User success change status product