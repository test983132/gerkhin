Feature: Seller Product


    Feature Product
    Background: Given User in homepage

@Positive_Case
Scenario: User wants to add product

        When Start to type your When step here User click "+Jual"
        And User fill in the fields correctly
        And User click "Terbitkan"
        And User successfully publish products
        Then User has successfully published the product

@Negative_Case
Scenario: User wants to add product

        When Start to type your When step here User click "+Jual"
        And User fill in the fields with the zero price column
        And User click "Terbitkan"
        And User successfully publish products
        Then User can't publish products with zero price

@Positive_Case
Scenario: User wants to reject the buyer's request

        When User click "Akun"
        And User click "Daftar Jual Saya"
        And User click kategori "Diminati"
        And User click "Tolak"
        And User direct to info penawar and "status order berhasil diperbaharui"
        Then User as seller can accepting bid the product

@Positive_Case
Scenario: User wants see own product

        When When User click "Akun"
        And User click "Daftar Jual Saya"
        And User click kategori "Diminati"
        And User click kategori "Terjual"
        And User click kategori "Produk"
        Then User can see all categories

@Positive_Case
Scenario: User wants to edit product

        When When User click "Akun"
        And User click "Daftar Jual Saya"
        And User click kategori "Produk"
        And User edit the product by nama produk, harga, photo and click "perbaharui produk"
        And User redirect to Daftar Jual Saya and product successfully edit
        Then User success change product